$(document).ready(function() {
    function validarFormulario() {
      // Obtener los valores de los campos del formulario
      const rut = $("#rut").val();
      const apellido_paterno = $("#apellido_paterno").val();
      const apellido_materno = $("#apellido_materno").val();
      const nombre = $("#nombre").val();
      const fecha_nacimiento = $("#fecha_nacimiento").val();
      const genero = $("#genero").val();
      const email = $("#email").val();
      const celular = $("#celular").val();
  
      // Validar el campo Rut
      if (rut.length < 9 || rut.length > 10) {
        alert("El Rut debe tener entre 9 y 10 caracteres.");
        return false;
      }
  
      // Validar el campo Apellido paterno
      if (apellido_paterno.length < 3 || apellido_paterno.length > 20) {
        alert("El Apellido paterno debe tener entre 3 y 20 caracteres.");
        return false;
      }
  
      // Validar el campo Apellido materno
      if (apellido_materno.length < 3 || apellido_materno.length > 20) {
        alert("El Apellido materno debe tener entre 3 y 20 caracteres.");
        return false;
      }
  
      // Validar el campo Nombre
      if (nombre.length < 3 || nombre.length > 20) {
        alert("El Nombre debe tener entre 3 y 20 caracteres.");
        return false;
      }
  
      // Validar el campo Fecha de nacimiento
      const hoy = new Date();
      const fechaNac = new Date(fecha_nacimiento);
      const edad = hoy.getFullYear() - fechaNac.getFullYear();
      const mes = hoy.getMonth() - fechaNac.getMonth();
  
      if (mes < 0 || (mes === 0 && hoy.getDate() < fechaNac.getDate())) {
        edad--;
      }
  
      // Validar el campo Noticias
      if (genero === "") {
        alert("Debes seleccionar una opción para Noticias que deseas recibir.");
        return false;
      }
  
      // Validar el campo Email
      if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)) {
        alert("Ingresa un email válido.");
        return false;
      }
  
      // Validar el campo Celular
      if (celular.length < 9 || celular.length > 12) {
        alert("El Celular debe tener entre 9 y 12 caracteres.");
        return false;
      }
  
      // Mostrar la alerta
      alert("Gracias, estás registrado en Caos News Noticias.");
  
      // Si se han validado todos los campos, devuelve true para enviar el formulario
      return true;
    }
  
    // Agregar evento de clic al botón de enviar formulario
    $("btn-enviar']").on("click", function(event) {
      event.preventDefault(); // Evitar el envío del formulario por defecto
      validarFormulario();
    });
  });
  