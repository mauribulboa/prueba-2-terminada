// obtener la referencia al elemento de la tabla
var tableBody = document.querySelector("#people-table tbody");

// obtener la referencia al input de búsqueda y al botón de búsqueda
var searchInput = document.querySelector("#search-input");
var searchButton = document.querySelector("#search-button");

// agregar un evento click al botón de búsqueda
searchButton.addEventListener("click", function() {
  // Obtener el valor de búsqueda
  var searchTerm = searchInput.value.toLowerCase().trim();

  // filtrar las filas de la tabla según el valor de búsqueda
  var rows = tableBody.querySelectorAll("tr");
  rows.forEach(function(row) {
    var rut = row.querySelector("td[data-rut]").getAttribute("data-rut").toLowerCase();
    var nombre = row.querySelector("td[data-nombre]").getAttribute("data-nombre").toLowerCase();
    var apellidoPaterno = row.querySelector("td[data-apellidoPaterno]").getAttribute("data-apellidoPaterno").toLowerCase();
    var apellidoMaterno = row.querySelector("td[data-apellidoMaterno]").getAttribute("data-apellidoMaterno").toLowerCase();

    if (
      rut.includes(searchTerm) ||
      nombre.includes(searchTerm) ||
      apellidoPaterno.includes(searchTerm) ||
      apellidoMaterno.includes(searchTerm)
    ) {
      row.style.display = "";
    } else {
      row.style.display = "none";
    }
  });
});
