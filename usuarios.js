document.addEventListener("DOMContentLoaded", function() {
  const loginBtn = document.getElementById("login-btn");
  loginBtn.addEventListener("click", function(e) {
    e.preventDefault();

    const emailInput = document.querySelector('input[type="email"]');
    const passwordInput = document.querySelector('input[type="password"]');
    const email = emailInput.value;
    const password = passwordInput.value;

    fetch("usuarios.json")
      .then(response => response.json())
      .then(data => {
        const users = data.users;
        const user = users.find(user => user.email === email && user.password === password);

        if (user) {
          // Autenticación exitosa
          alert("Inicio de sesión exitoso");
          window.location.href = "indexTabla.html"; // Redirige al usuario a index.html
        } else {
          // Autenticación fallida
          alert("Credenciales inválidas. Por favor, intente nuevamente.");
        }
      })
      .catch(error => {
        console.log("Error al leer el archivo json:", error);
      });
  });
});
