// obtener la referencia al elemento de la tabla
var tableBody = document.querySelector("#people-table tbody");

// obtener los datos del archivo personas.json
fetch("personas.json")
  .then(function(response) {
    return response.json();
  })
  .then(function(data) {
    // recorrer los datos y crear las filas de la tabla
    data.personas.forEach(function(persona) {
      var row = document.createElement("tr");
      row.innerHTML = `
        <td data-rut="${persona.Rut}">${persona.Rut}</td>
        <td data-nombre="${persona.Nombre}">${persona.Nombre}</td>
        <td data-apellidoPaterno="${persona["Apellido Paterno"]}">${persona["Apellido Paterno"]}</td>
        <td data-apellidoMaterno="${persona["Apellido Materno"]}">${persona["Apellido Materno"]}</td>
        <td>${persona["Fecha Nacimiento"]}</td>
        <td>${persona.Email}</td>
        <td>${persona["Numero Contacto"]}</td>
      `;
      tableBody.appendChild(row);
    });
  })
  .catch(function(error) {
    console.log("Error al cargar los datos:", error);
  });

